// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let waktuBaca = 10000
let time;
let idBuku = 0;

function baca(){
    readBooks(waktuBaca, books[idBuku], function(sisaWaktu){
        time = waktuBaca;
        waktuBaca = sisaWaktu;
        idBuku++;
        if (idBuku < books.length && waktuBaca < time)
        {
        baca();
        }
    });
}

baca();

