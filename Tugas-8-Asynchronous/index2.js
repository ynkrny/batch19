var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let waktuBaca = 10000;
let idBuku = 0;

function baca()
{
    readBooksPromise (waktuBaca, books[idBuku])
        .then(function (sisaWaktu)
        {
            waktuBaca = sisaWaktu;
            idBuku++;
            if (idBuku < books.length)
            {
                baca();
            }
        })
        .catch(function(error){})
}

baca();
