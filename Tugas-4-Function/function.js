console.log("NO 1")

function teriak(){
    var string = "Halo Sanbers";
    return string
}
 
console.log(teriak()) // "Halo Sanbers!" 

console.log("\n")
console.log("NO 2")

function kalikan(angka1, angka2){
    return angka1 * angka2
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) 

console.log("\n")
console.log("NO 3")

function introduce(name, age, address, hobby){
    var string = "Nama saya "+ name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby
    return string
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)