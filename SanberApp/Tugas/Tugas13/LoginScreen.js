import React, {Component} from 'react'
import {View, Text, Image } from 'react-native'

class Home extends Component{
    render() {
        return (
            <View>
                <Image style={{height: 50, marginTop:50}} source={require('./images/logo.png')} />
                <Text>Portofolio</Text>
            </View>
        )
    }
}

export default Home