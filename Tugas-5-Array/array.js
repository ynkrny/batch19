console.log('No.1')
console.log('=============')

function range (startNum, finishNum) {
    let dsc = []
    if (startNum < finishNum) {
        for(let i = startNum; i <= finishNum; i ++) {
            dsc.push(i)
        }
    } else if (startNum > finishNum) {
        for(let i = startNum; i >= finishNum; i --) {
            dsc.push(i)
        }
    } else dsc = -1 
    return dsc
} 
console.log('')
console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

console.log('')
console.log('No.2')
console.log('=============')

function rangeWithStep(startNum, finishNum, step) {
    let dsc = []
    if (startNum < finishNum) {
        for(let i = startNum; i <= finishNum; i += step) {
            dsc.push(i)
        }
    } else if (startNum > finishNum) {
        for(let i = startNum; i >= finishNum; i -= step) {
            dsc.push(i)
        }
    } else dsc = -1 
    return dsc
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log('')
console.log('No.3')
console.log('=============')

//belum bisa

console.log('')
console.log('No.4')
console.log('=============')

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

//belum bisa

console.log('')
console.log('No.5')
console.log('=============')

function balikKata(word){
    var kata = '';
    for(var i = word.length -1; i>=0; i--){
        kata += word[i];
    } return kata;
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))


console.log('')
console.log('No.6')
console.log('=============')

//var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
//dataHandling2(input);
 
//belum bisa