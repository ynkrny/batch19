console.log('No. 1 Looping While');
console.log("====================");

    var deret = 1;
    var jumlah = 20;

    console.log("LOOPING PERTAMA")
    while(deret <= 10) { // Loop akan terus berjalan selama nilai deret masih di atas 0
    jumlah = deret *2 ; // Menambahkan nilai variable jumlah dengan angka deret
    deret++; // Mengubah nilai deret dengan mengurangi 1
    console.log(jumlah + " - I love coding");
    }

    console.log("=============");
    console.log("LOOPING KEDUA");

    var deret2 = 22;
    var jumlah2 = 1;

    while(deret2 >= 4){
        jumlah2 = deret2 -2;
        deret2 -=2;
        console.log(jumlah2 + '- I will become a mobile developer')
    }

console.log("====================");
console.log('No. 2 ');
console.log("====================");

var jawaban2 = '';

for (var i = 1; i <=20; i++) {
    
    if (i % 2 == 1 && i % 3 !=0) {
        console.log(jawaban2 = i + ' - Santai');
    } else if (i % 2 ==0) {
        console.log(jawaban2 = i + ' - Berkualitas');
    } else if (i % 2 == 1 && i % 3 == 0) {
        console.log(jawaban2 = i + ' - I Love Coding');
    }
}

console.log("====================");
console.log('No. 3 ');
console.log("====================");

for (x=1; x<=4; x++){
    console.log('########')
}

console.log("====================");
console.log('No. 4 ');
console.log("====================");

var totalTangga = 7;
var tangga = '#';
var x = 0;
var y = 0;

while (x < totalTangga) {
    while (y < totalTangga) {
      y++;
      tangga += '';
    }
    console.log(tangga);
    tangga += '#';
    x++;
  }


console.log("====================");
console.log('No. 5 ');
console.log("====================");

for (x=1; x<=8; x++)
{
   if ((x%2)==1){
     console.log (' # # # #')
   }
   else if ((x%2)==0){
     console.log ('# # # #')
   } 
}
