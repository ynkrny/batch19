console.log("====================");
console.log("1. Animal Class")
console.log("===Soal No.1===");
class Animal {
    constructor(name) {
        this.aname = name;
        this.alegs = 4;
        this.acold_blooded = false;
    }

    get name() {
        return this.aname;
    }

    set name(x) {
        this.aname = x;
    }

    get legs() {
        return this.alegs;
    }
    
    set legs(x) {
        this.alegs = x;
    }

    get cold_blooded() {
        return this.acold_blooded;
    }
    
    set cold_blooded(x) {
        this.acold_blooded = x;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


//Release 1
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name) {
        
    }
} 


var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 



console.log("====================");
console.log("2. Function to Class")



function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 

  class Clock {
    // Code di sini
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  